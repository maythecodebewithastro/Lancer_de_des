class Dice {
  constructor() {
    this.number = 0;
  };

  computeNumber(diceCfg) {
    // etape 1 tirer un nombre au hasard (random = nombre reel, entre 0 et 1)
    this.number = Math.random();
    // etape 2 transmformer le nombre
    this.number = this.number * (diceCfg.faces - 1);
    this.number = Math.round(this.number);
    this.number = this.number + 1;
  }
}
