class BoardUI {
  constructor() {
    this._divBoard = $('#board');
    this._divDices = [ ];
    this.dices = [ ];
  }

  init() {
    let curId;
    $('#play').on('click', this.launchDices.bind(this));
    for (let nbrDices = 0; nbrDices < CONFIG_DICES.count; nbrDices++) {
      curId = 'dice' + nbrDices;
      this._divBoard.append('<div class="col" id="' + curId + '"></div>');
      this._divDices[nbrDices] = this._divBoard.find('[id="' + curId +'"]');
      // creer un objet DiceUI pour chaque des
      this.dices.push(new DiceUI(curId, this._divDices[nbrDices]));
      this.dices[nbrDices].display();
    };
  };

  launchDices() {
    for (let nbrDices = 0; nbrDices < this.dices.length; nbrDices++) {
      this.dices[nbrDices].launch();
    };
  };
};
