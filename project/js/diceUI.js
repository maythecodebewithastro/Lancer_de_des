
/*
<div class="col dice">
  <select class="form-select" id="diceType1">
  </select>
</div>
*/

class DiceUI extends Dice {
  constructor(parId, eltDice) {
      super();
      this._divDice = eltDice;
      this.parentId = parId;
  };

  display() {
    let content = '';
    content += '<div class="row">';
    content += '<div class="col dice"><select class="form-select form-control" id="' + this.parentId + 'Select">';
    content += this.configDicesChoices();
    content += '</select></div>';
    content += '</div>';
    content += '<div class="row">';
    content += '<div class="col">';
    content += '<div class="square square' + this.parentId + '"></div>';
    content += '</div>';
    content += '<div class="chekbox">';
    content += '<input type="checkbox" checked id="' + this.parentId + 'check">';
    content += '<label> actif';
    content += '</label>';
    content += '</div>';
    this._divDice.html(content);
    this.attachCheckboxEvent();
  };

  attachCheckboxEvent() {
    $('#' + this.parentId + 'check').on('change', this.activateDice.bind(this));
  };

  activateDice() {
      $('#' + this.parentId + 'Select').attr('disabled', !$('#' + this.parentId + 'check').is(':checked'));
  };

  configDicesChoices() {
    let options = '';
    for (let pos = 0; pos < CONFIG_DICES.dices.length; pos++) {
        options += '<option value="' + CONFIG_DICES.dices[pos].id + '"';
        if (CONFIG_DICES.dices[pos].default) {
          options += ' selected=""';
        }
        options += '>';
        options += CONFIG_DICES.dices[pos].lib;
        options += '</option>';
    }
    return options;
  }

  displayFace() {
    $('.square' + this.parentId).html(($('#' + this.parentId + 'check').is(':checked')? this.number : ''));
  };

  launch() {
    let idSelectDice;
    let valOption;

    // etape 0: controler que le de est actif
    if ($('#' + this.parentId + 'check').is(':checked')) {
      // etape 1 trouver le type de dé
      // - calculer l'id du dé (idSelectDice)
      idSelectDice = '#' + this.parentId + 'Select';
      // - chercher l'option choisie dnas le select dont l'id est idSelectDice
      valOption = $(idSelectDice).val();
      // - chercher le dé dans CONFIG_DICES dont l'id est la valeur selectionnee
      for (let pos = 0; pos < CONFIG_DICES.dices.length; pos++) {
        if (CONFIG_DICES.dices[pos].id === valOption) {
            this.computeNumber(CONFIG_DICES.dices[pos]);
        }
      }
    }
    // TODO this.displayFace();
    this.displayFace();
  }
}
