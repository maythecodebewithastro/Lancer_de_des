let CONFIG_DICES = {
  count: 6,//le nombre de dés
  dices: [
    { faces: 2, default: false, lib: 'pile ou face', id: 'dice2'},
    { faces: 4, default: false, lib: 'pyramide', id: 'dice4'},
    { faces: 6, default: true, lib: 'Dé standard', id: 'dice6' },
    { faces: 8, default: false, lib: 'Dé à 8 faces', id: 'dice8' },
    { faces: 10, default: false, lib: 'Dé à 10 faces', id: 'dice10' },
    { faces: 12, default: false, lib: 'Dé à 12 faces', id: 'dice12' },
    { faces: 20, default: false, lib: 'Dé à 20 faces', id: 'dice20'}
  ]
};
